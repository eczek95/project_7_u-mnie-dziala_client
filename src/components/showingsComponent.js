import { Link } from 'react-router-dom';

const ShowingsComponent = ({ id, setVip, seats, tickets, vipSeats, vipTickets, playingDate, pickedFilm, setChosenShowingId }) => {
	return (
		<div className="flex rounded-3xl text-white gap-8 w-6/12 h-64 mx-auto p-8 bg-gradient-to-l from-red-900 to-black">
			<div style={{ backgroundImage: `url(${pickedFilm[0].cover})` }} className="w-40 h-full bg-cover"/>
			<div className="flex-1 flex flex-col justify-between">
				<h1 className="font-bold text-3xl">{pickedFilm[0].title}</h1>
				<h2 className="font-semibold">{playingDate.slice(5, 10).replace('-', '.')} {playingDate.slice(11, 16)}</h2>
				<div className="flex gap-4">
					<div className="flex items-center border-r-2 pr-4 font-semibold">
						Bilet
					</div>
					<div>
						<p>Normalny: {(pickedFilm[0].price / 100).toFixed(2)} zł</p>
						<p>VIP: {(pickedFilm[0].price / 100 * 1.1).toFixed(2)} zł</p>
					</div>
				</div>
				<div className="flex gap-8">
					<Link to="/buyTicket">
						<button disabled={seats - tickets === 0} onClick={() => setChosenShowingId(id)} className="disabled:opacity-40 disabled:cursor-default px-4 py-2 rounded-3xl bg-red-900 w-24">Kup Bilet
						</button>
					</Link>
					<Link to="/buyTicket">
						<button disabled={vipSeats - vipTickets === 0} onClick={() => {setVip(true); setChosenShowingId(id)}}
										className="disabled:opacity-40 disabled:cursor-default px-4 py-2 text-yellow-400 rounded-3xl bg-red-900 w-24">VIP
						</button>
					</Link>
				</div>
			</div>
			<div className="flex flex-col gap-4">
				<div className="text-yellow-200 flex items-center gap-2 self-end">
					<img src="/star.png" className="w-4 h-4" alt="Star"/> {pickedFilm[0].rating} / 100
				</div>
				<div className="bg-black flex flex-col justify-center gap-4 opacity-40 inline-block px-2 flex-1">
					<p className="border-b border-white text-center pb-4">Dostępne miejsca: {seats + vipSeats - tickets - vipTickets}</p>
					<div className="flex justify-evenly gap-3">
						<div className="text-center self-center">
							<h3 className="">Normalne</h3>
							{seats - tickets}
						</div>
						<div className="text-center flex-1">
							<h3 className="text-yellow-400">VIP</h3>
							{vipSeats - vipTickets}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ShowingsComponent;