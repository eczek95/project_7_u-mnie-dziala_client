import { Link } from 'react-router-dom';

const Film = ({ title, description, cover, rating, choseFilm, availableMovies }) => {
	return (
		<div className="h-full">
			<div style={{backgroundImage: `url(${cover})`}} className="w-10/12 relative flex flex-col bg-cover h-96 mx-auto py-10 px-10 bg-white rounded-3xl">
				<div style={{backgroundColor: 'rgba(0,0,0,0.5)'}} className="absolute left-0 top-0 rounded-3xl w-full h-full"/>
				<div style={{backgroundColor: 'rgba(0,0,0,0.3)'}} className="text-white absolute z-10 p-3 rounded-xl self-end flex flex-col items-end gap-5">
					<div className="w-60 text-right flex flex-col gap-4">
						<h2 className="font-bold text-3xl text-center">{title}</h2>
						<div className="text-justify">{description}</div>
						<div className="text-yellow-200 flex justify-end items-center gap-2"><img src="/star.png" className="w-4 h-4" alt="Star"/> {rating} / 100</div>
					</div>
					<Link to="/showings">
						<button type="button" onClick={() => choseFilm(availableMovies.filter(item => item.title === title))} className="bg-white text-black font-bold w-40 px-6 py-1.5 rounded-xl inline uppercase">Sprawdź</button>
					</Link>
				</div>
			</div>
		</div>
	);
};
//
// <div>
// 	<div>Cena</div>
// 	<div>{(parseInt(price) / 100).toFixed(2)} zł</div>
// </div>
// <div>
// 	<div>Cena VIP</div>
// 	<div>{(parseInt(price) * 1.1 / 100).toFixed(2)} zł</div>
// </div>

export default Film;